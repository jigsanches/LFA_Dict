/*
 parser.c : analisador sintatico (exemplo de automato com pilha)
 Autor: Edna A. Hoshino

 S  -> Function S_ 
 S_ -> Function S_ 
      | epsilon
 Function -> Type Function_
 Type -> void 
       | int 
       | float
 Function_ -> main() { D B } 
            | id() { D B }
 D  -> Type L; D | epsilon
 L  -> id A L'
 L' -> , L | epsilon
 A  -> = E | epsilon
 B  -> C B 
      | epsilon
 C  -> id = E ; 
      | while (E) C 
      | { B }
 E  -> TE'
 E' -> +TE' 
      | epsilon
 T  -> FT'
 T' -> *FT' 
      | epsilon
 F  -> (E) 
      | id 
      | num
***********************MINHAS ALTERAÇÕES*******************
 C -> id = X;
      | idM = V;
      | while (E) C 
      | { B }
 X -> E
    | G 
 G -> {I}
 I -> K : V 
    |K : V , I
 K -> E
    | W
 W -> W + W 
    | "id"
    | id
    | idm
 V -> G | K
 M -> [K]
    |[K]M
 F  -> (E) 
      | idM
      | num

**********************TRANSFORMANDO EM LL1******************
 C  -> idH;
      | while (E) C 
      | { B }
 H  -> [K]MN
      | J
 N  -> = V
      | epsilon
 J  -> = X | epsilon
 M  -> [K]M | epsilon
 X  -> E
      | {I}
 I  -> K : V I'
 I' -> , I | epsilon
 K  -> (K) 
      | idM K' 
      | num K'' 
      | String W
 K' -> +K 
      | *E
      | epsilon
 K''-> +E 
      | *E 
      | epsilon
 W  -> + W' 
      | epsilon
 W' -> idM W
      | String W
 V  -> {I}
      | K 
 F  -> (E) 
      | idM
      | num

***********************************************************
 K -> (E)T'E' 
      | id K'
      | numT'E'
      | String W
 K'-> *FT'N' | + N | epsilon
 N -> W' | TE'
 N'-> +TE' | epsilon

 A saida do analisador apresenta o total de linhas processadas e uma mensagem de erro ou sucesso. 
 Atualmente, nao ha controle sobre a coluna e a linha em que o erro foi encontrado.
*/

#include "lex.h"
#include "parser.h"
#include <stdio.h>
#include <stdlib.h>

/* variaveis globais */
int lookahead;
FILE *fin;

int lex();
void S();
void S_();
void Function();
void Type();
void Function_();
void B();
void C();
void E();
void T();
void F();
void E_();
void T_();
void D();
void L();
void L_();
void A();
/*CRIANDO AS FUNÇÕES PARA AS VARIÁVEIS DO DICTIONARY*/
void X();
void I();
void I_();
void K();
void W();
void W_();
void V();
void M();
void J();
void H();
void K_();
void K__();
void N();
/*****************************************************/
void match();

//meu cod ***********************************************************
void X(){
  if(lookahead==ABRE_CHAVES){
    match(ABRE_CHAVES);
    I();
    match(FECHA_CHAVES);
  }
  else{
    E();
    /*match(PONTO_VIRG);*/
  }
}
void I(){
  K();
  match(DOT_DOT);
  V();
  I_();
}
void K(){
  if(lookahead==ABRE_PARENT){
    match(ABRE_PARENT);
    K();
    match(FECHA_PARENT);
  }
  else if(lookahead==ID)
  {
    match(ID);
    M();
    K_();
  }
  else if(lookahead==NUM)
  {
    match(NUM);
    K__();
  }
  else{
    match(STRING);
    W();
  }
}
void K_()
{
  if(lookahead==OP_ADIT)
  {
    match(OP_ADIT);
    K();
  }
  else if(lookahead==OP_MULT)
  {
    match(OP_MULT);
    E();
  }
}
void K__()
{
  if(lookahead==OP_ADIT)
  {
    match(OP_ADIT);
    E();
  }
  else if(lookahead==OP_MULT)
  {
    match(OP_MULT);
    E();
  }
}
void V(){
  if(lookahead==ABRE_CHAVES){
    match(ABRE_CHAVES);
    I();
    match(FECHA_CHAVES);
  }
  else{
    K();
  }
}
void I_(){
  if(lookahead==VIRG){
    match(VIRG);
    I();
  }
}
void W(){
  if(lookahead==OP_ADIT){
    match(OP_ADIT);
    W_();
  }
}
void W_(){
  if(lookahead==ID){
    match(ID);
    M();
  }
  else{
    match(STRING);
  }
  W();
}
void M(){
  if(lookahead==ABRE_COLC){
    match(ABRE_COLC);
    K();
    match(FECHA_COLC);
    M();
  }
}
void J(){
  if(lookahead==OP_ATRIB){
    match(OP_ATRIB);
    X();
  }
}
void H()
{
  if(lookahead==ABRE_COLC)
  {
    match(ABRE_COLC);
    K();
    match(FECHA_COLC);
    M();
    N();
  }
  else
  {
    J();
  }
}
void N()
{
    if(lookahead==OP_ATRIB)
    {
        match(OP_ATRIB);
        V();
    }
}
void C(){
  if(lookahead==ID){
    match(ID);
    H();
    match(PONTO_VIRG);
  }
  else if(lookahead==WHILE){
    match(WHILE);
    match(ABRE_PARENT);
    E();
    match(FECHA_PARENT);
    C();
  }
  else if(lookahead==ABRE_CHAVES){
    match(ABRE_CHAVES);
    B();
    match(FECHA_CHAVES);
  }
}
//meu cod FIM*********************************************************
void D(){
  if(lookahead==INT || lookahead==FLOAT || lookahead==VOID){
    Type();
    L();
    match(PONTO_VIRG);
    D();
  }
}

void L(){
  match(ID);
  A();
  L_();
}

void L_(){
  if(lookahead==VIRG){
    match(VIRG);
    L();
  }
}

void A(){
  if(lookahead==OP_ATRIB){
    match(OP_ATRIB);
    E();
  }
}

void match(int t)
{
  if(lookahead==t){
    lookahead=lex();
  }
  else{
    printf("\nErro: token %s (cod=%d) esperado.## Encontrado \"%s\" ##\n", terminalName[t], t, lexema);
    exit(1);
  }
}

void S(){
  Function();
  S_();
}

void S_(){
  if(lookahead==INT || lookahead==FLOAT || lookahead==VOID){
    Function();
    S_();
  }
}

void Function(){
  Type();
  Function_();
}

void Type(){  
  if(lookahead==INT){
    match(INT);
  }
  else if(lookahead==FLOAT){
    match(FLOAT);
  }
  else{
    match(VOID);
  }
}

void Function_(){
  if(lookahead == MAIN){
    match(MAIN);
    match(ABRE_PARENT);
    match(FECHA_PARENT);
    match(ABRE_CHAVES);
    D();
    B();
    match(FECHA_CHAVES);
  }
  else{
    match(ID);
    match(ABRE_PARENT);
    match(FECHA_PARENT);
    match(ABRE_CHAVES);
    D();
    B();
    match(FECHA_CHAVES);
  }
}

void B(){
  if(lookahead==ID || lookahead==WHILE || lookahead==ABRE_CHAVES){
    C();
    B();
  }
}
/*void C(){
  if(lookahead==ID){
    match(ID);
    match(OP_ATRIB);
    E();
    match(PONTO_VIRG);
  }
  else if(lookahead==WHILE){
    match(WHILE);
    match(ABRE_PARENT);
    E();
    match(FECHA_PARENT);
    C();
  }
  else if(lookahead==ABRE_CHAVES){
    match(ABRE_CHAVES);
    B();
    match(FECHA_CHAVES);
  }
}*/

void E(){
  T();
  E_();
}

void T(){
  F();
  T_();
}
void E_(){
  if(lookahead==OP_ADIT){
    match(OP_ADIT);
    T();
    E_();
  }
}
void F(){
  if(lookahead==ABRE_PARENT){
    match(ABRE_PARENT);
    E();
    match(FECHA_PARENT);
  }
  else{
    if(lookahead==ID){
      match(ID);
      M();
    }
    else
      match(NUM);
  }
}
void T_(){
  if(lookahead==OP_MULT){
    match(OP_MULT);
    F();
    T_();
  }
}

/*******************************************************************************************
 parser(): 
 - efetua o processamento do automato com pilha AP
 - devolve uma mensagem para indicar se a "palavra" (programa) estah sintaticamente correta.
********************************************************************************************/
char *parser()
{
  lookahead=lex();
  S();
  if(lookahead==FIM)
    return("Programa sintaticamente correto!");
  else
    return("Fim de arquivo esperado");
}

int main(int argc, char**argv)
{
  if(argc<2){
    printf("\nUse: compile <filename>\n");
    return 1;
  }
  else{
    printf("\nAnalisando lexica e sintaticamente o programa: %s", argv[1]);
    fin=fopen(argv[1], "r");
    if(!fin){
      printf("\nProblema na abertura do programa %s\n", argv[1]);
      return 1;
    }
    printf("\nTotal de linhas processadas: %d\nResultado: %s\n", lines, parser());
    fclose(fin);
    return 0;
  }
}
