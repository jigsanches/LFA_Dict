int t()
{
	int a;
	dic1 = { "string" : "string", "string" : 10, "string" : a };
	dic2 = { 10 : "string", 10 : 11, 10 : a };
	dic3 = { a : "string", a : 10, a : b };
	dic4 = { "string" : { "string" : "string", "string" : 10, "string" : a }, "string" : { 10 : "string", 10 : 11, 10 : a }, "string" : { a: "string", a : 10, a : b } };
	dic5 = { 12 : { "string" : "string", "string" : 10, "string" : a }, 12 : { 10 : "string", 10 : 11, 10 : a }, 12 : { a: "string", a : 10, a : b } };
	dic6 = { c : { "string" : "string", "string" : 10, "string" : a }, c : { 10 : "string", 10 : 11, 10 : a }, c : { a: "string", a : 10, a : b } };
	dic7 = { "string" : { "string" : { "string" : "string", "string" : 10, "string" : a }, "string" : { 10 : "string", 10 : 11, 10 : a }, "string" : { a: "string", a : 10, a : b } } };
	dic8 = { 13 : { 12 : { "string" : "string", "string" : 10, "string" : a }, 12 : { 10 : "string", 10 : 11, 10 : a }, 12 : { a: "string", a : 10, a : b } } };
	dic9 = { d: { c: { "string" : "string", "string" : 10, "string" : a }, c : { 10 : "string", 10 : 11, 10 : a }, c : { a: "string", a : 10, a : b } } };
	dic10 = { "string" + "string" : "string" + "string", "string" + "string" : 10 * 20 + 7 * 5 + 5 + 500 * 2 * 100, "string" + "string" : a * b + c * d + d + e * f * g };
	dic11 = { 10 * 20 + 7 * 5 + 5 + 500 * 2 * 100 : "string" + "string", 10 * 20 + 7 * 5 + 5 + 500 * 2 * 100 : 10 * 20 + 7 * 5 + 5 + 500 * 2 * 100, 10 * 20 + 7 * 5 + 5 + 500 * 2 * 100 : a * b + c * d + d + e * f * g };
	dic12 = { a * b + c * d + d + e * f * g : "string" + "string", a * b + c * d + d + e * f * g : 10 * 20 + 7 * 5 + 5 + 500 * 2 * 10, a * b + c * d + d + e * f * g : a * b + c * d + d + e * f * g };
	dic13 = { "string" + "string" + z + y + "string" + x :  "string" + "string" + z + y + "string" + x, a + 10 * b + c * 40 * d * e + 8 : a + 10 * b + c * 40 * d * e + 8 };
	dic1["string"] = a + b + c + d * e * f * g * h;
	dic2[10] = 10 + 40 * 5 * 7 * 100 * 35;
	dic3[a] = "string" + "string" + "string" + "string";
	dic4["string"] = { "string" : 10 * 20, "string" + "string" : a + b };
	dic5[12]["string"] = z + "string" + "string" + y + x;
	dic7[c][12 + e * 5] = { "string" : 10 * 20, "string" + "string" : a + b };
	dic8["string" + z + "string"] = { "string" : { "string" : 10 * 20, "string" + "string" : a + b }, "string" + "string" : a + b };
	dic9[z + y + x]["string" + "string" + z + y + "string"][a + 10 * b + c * 40 * d * e + 8] = 10;
	dic10[a][b][c][d][e][f][g][h][i][j][k][l][m][n][o][p][q][r][s][t][u][v][w][x][y][z] = dic9[z + y + x]["string" + "string" + z + y + "string"][a + 10 * b + c * 40 * d * e + 8];
	a = dic11[10 * 20 + 7 * 5 + 5 + 500 * 2 * 100] + dic12[a * b + c * d + d + e * f * g] * dic13[a + 10 * b + c * 40 * d * e + 8];
}
